---
layout: post
title: "Xamarin Studio で複数のソリューションを開く方法"
date: 2013-07-28 13:52
comments: true
categories: [Xamarin]
---
わかりにくいのでメモっておく。
<!--more-->
### 1. ソリューションをひとつ開く

### 2. ２つ目のソリューションを開く 

![img1](https://dl.dropboxusercontent.com/u/264530/qiita/open_multi_solution_in_xamarin_studio_01.png) この時、「現在のワークスペースを閉じる」のチェックを **外す**

### 3. すると２つ目のソリューションが追加されてます。 

![img1](https://dl.dropboxusercontent.com/u/264530/qiita/open_multi_solution_in_xamarin_studio_02.png)

ソリューションを新規作成する時には行えないようです。
分かりにくいよママン。