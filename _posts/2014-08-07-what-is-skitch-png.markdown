---
layout: post
title: "「Skitch PNG」とはなにか？"
date: 2014-08-07 23:42:38 +0900
comments: true
categories: [skitch]
---
画像に文字や図形を描くとき多用している Skitch ですが、ファイルのエクスポートに「Skitch PNG」という謎のファイル形式があるのを発見。
<!--more-->
![](https://dl.dropboxusercontent.com/u/264530/qiita/skitch_png_01.png)

調べてみると、

* [Skitch For Mac Updated With A Format That Keeps Annotations Editable](http://www.addictivetips.com/mac-os/skitch-updated-with-a-special-format-that-keeps-annotations-editable/)

> Skitch PNG is a new format for saving pictures that you’ve annotated in Skitch. These are almost like regular PNGs, except that you can open them in Skitch later on and edit the previous annotations. 

な、なんだってー！PNG なのに **「アノテーションが再編集可能」** だってー！！

## 試しに

![](https://dl.dropboxusercontent.com/u/264530/qiita/skitch_png_02.png)

[photo by Nesster, CC-BY](http://www.gatag.net/10/16/2009/110000.html)

上の画像、「Skitch PNG」で保存したものですが、Skitch で開き直すと、下図のように文字や矢印が再編集できます。

![](https://dl.dropboxusercontent.com/u/264530/qiita/skitch_png_03.png)

これからはこれをデフォで使っていこうと思います。
ちなみに「モザイク」は復元できないみたいです。うむ安全設計。

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=oku2008-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4774156019" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
