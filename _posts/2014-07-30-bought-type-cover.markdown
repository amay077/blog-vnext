---
layout: post
title: "タイプカバーを買いました"
date: 2014-07-30 15:27:57 +0900
comments: true
categories: [Surface]
---

[先日買った Surface Pro 3](http://blog.amay077.net/blog/2014/07/18/welcome-surface-pro-3/) 用のタイプカバーを買いました。
<!--more-->
![](https://dl.dropboxusercontent.com/u/264530/qiita/bought_type_cover_01.jpg)

[Xamarin](http://xamarin.com) カラーの **シアン！**

キーボードの打感は、個人的には良好。普段 MacBookAir やApple純正のあのうす～いキーボードに慣れているので、なんなく順応できますね。

これでお出かけ時に持っていけるぞ。

こうなってくると次は外出時のバッテリー問題を解決したい！
MacBookAir には、[HyperJuice](http://www.amazon.co.jp/gp/product/B00456DWUA/ref=as_li_ss_tl?ie=UTF8&camp=247&creative=7399&creativeASIN=B00456DWUA&linkCode=as2&tag=oku2008-22) を使っていて、実質半日くらいは持つのですが、こいつで Surface にも給電できないかなーと思ってます。変換コネクタみたいなのでないかな？

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=oku2008-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00KQ5CKCA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=oku2008-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00KQ5A9KA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=oku2008-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00456DWUA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
