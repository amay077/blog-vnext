---
layout: post
title: "ARカーナビとな？"
date: 2010-11-15 8:35
comments: true
categories: [AR]
---
<!--more-->

<iframe src="http://player.vimeo.com/video/11870382" width="400" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

カッコいいなあ。 やりたいなあ。たぶんできないけど。
必要なものを推測してみる。

* 道路ネットワークデータ
* GPS
* カメラ
* カメラ（目線）の高さ
* カメラの角度
* 方位角

こんなもんで、映像に道路を重ねられるのかな？

これができたら、さ、カーナビの3D表示とか要らないね。

あと振動センサと連動させてドライブレコーダーにもなりそうだし。

いろいろアイデア膨らむなぁ、たぶんどこかが既に着手してるんだろうな。