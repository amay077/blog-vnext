---
layout: post
title: "Google API Console の API Access って…"
date: 2013-10-30 20:00
comments: true
categories: [Android, GoogleMapsAPI]
---
[Google API Console](https://code.google.com/apis/console/) の API Access って、複数のアプリを登録できたんですね。気づかなかった。。。
<!--more-->
![img1](https://dl.dropboxusercontent.com/u/264530/qiita/multi_apps_in_google_api_console_01.png)

>\<SHA1フィンガープリント>;<アプリのパッケージ名><改行><br/>
>\<SHA1フィンガープリント>;<アプリのパッケージ名>

って書けばよかったんだ。

新しくなった [Cloud Console](https://cloud.google.com/console) では、 +/- ボタンが付いて分かりやすい。

![img1](https://dl.dropboxusercontent.com/u/264530/qiita/multi_apps_in_google_api_console_02.png)

