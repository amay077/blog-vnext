---
layout: post
title: "MVP アワードキットが届きました"
date: 2016-10-12 02:08:57 +0900
comments: true
categories: [Microsoft, MVP]
---

海の向こうから、「MVPアワードキット」が届きましたよっと。

<!--more-->

### 箱！

![](https://dl.dropboxusercontent.com/u/264530/qiita/received_mvp_award_kit_01.jpg)

### 中身！！

![](https://dl.dropboxusercontent.com/u/264530/qiita/received_mvp_award_kit_02.jpg)

* 盾
* 置物
* メンバーカードみたいなやつ
* ピンバッジ
* 説明書みたいなやつ

です。

この他には書類とか MVP のステッカーとか。さっそく（会社の）MacBook Pro に貼ってやったぜぇー。

MVPのセンパイ方も、初めて受賞したときはこういう事したんだろうなーと思いながら自分もやってしまったｗ
