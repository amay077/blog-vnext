---
layout: post
title: "Sublime Text 2 で箱型選択する"
date: 2013-03-29 14:12
comments: true
categories: [SublimeText2]
---
Mac の場合、「**``option`` キーを押しながらマウスをドラッグ**」でした。
<!--more-->
キーボードでやるとか、Windows の人は、

* [Column Selection - Sublime Text 2 Documentation](http://www.sublimetext.com/docs/2/column_selection.html)

を見てください。

!["1"](https://dl.dropbox.com/u/264530/qiita/sublimetext2_boxselection_1.png)

切り取った後は、左詰めされます。

!["2"](https://dl.dropbox.com/u/264530/qiita/sublimetext2_boxselection_2.png)
