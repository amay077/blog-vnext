---
layout: page
title: About
permalink: /about/
---

名前:amay077(あめいと読むらしいぞ)

[![MVP](/assets/images/mvp_logo_h.png)](https://mvp.microsoft.com/ja-jp/PublicProfile/5002207)

Microsoft MVP for Visual Studio and Development Technologies(っていうか Xamarin) 2016/10-

位置情報モバイルアプリエンジニア。C++ → VB → C# → Android とか HTML5 とか Rx とか MVVM とか OpenData とか。

Xamarin.Forms 向けの地図ライブラリ「[Xamarin.Forms.GoogleMaps](https://github.com/amay077/Xamarin.Forms.GoogleMaps)」を開発・[公開](https://www.nuget.org/packages/Xamarin.Forms.GoogleMaps/)しています。

Build Insider にて、 [Xamarin逆引きTips](http://www.buildinsider.net/mobile/xamarintips) を連載してます、よろしくどうぞ。

よゐこ濱口に似てるらしいです。

詳しいプロフィールは Wantedly のプロフィールに書いたので、見たい方は twitter などでご連絡ください。

## Where are you?

### Twitter

ほぼいつも居ます。

* https://twitter.com/amay077

### Qiita

Xamarin や Android などの開発ネタを投稿してます。

* http://qiita.com/amay077

### GitHub

自作OSSプログラムやサンプルコードなどを公開してます。

* https://github.com/amay077

### Q&Aサイト

Xamarin に関する質問はウォッチしてるので気軽に投稿してください。

* [teratail](https://teratail.com/users/amay077)
* [スタックオーバーフロー](http://ja.stackoverflow.com/users/15190/amay077)

### 作ったアプリ

Android 向けに作ったアプリを公開してます。

* [Google Play store](https://play.google.com/store/apps/developer?id=amay077&hl=ja)

### 発表資料

勉強会などで登壇・発表した資料です。

* [Slideshare](http://www.slideshare.net/amay077)
* [Speaker Deck](https://speakerdeck.com/amay077)
